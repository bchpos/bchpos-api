import time

import dataset
import requests

SOURCES = {
    "bitso": ["MXN"]
}


def is_supported(currency, source):
    if source in SOURCES:
        if currency in SOURCES[source]:
            return True

    return False


def get_currencies(source):
    if source in SOURCES:
        return {"currencies": SOURCES[source]}


def get_sources():
    _keys = []
    for key in SOURCES:
        _keys.append(key)

    return {"sources": _keys}


def update_db(source, currency):
    _db = dataset.connect()
    _table = _db[source]

    _api = "https://api.bitso.com/v3/ticker/?book=bch_mxn"

    print("Fetching price: " + _api)
    _response = requests.get(_api)
    _json = _response.json()

    _record = _table.find_one(currency=currency)
    if not _record:
        print("Inserting price in database: " + currency)
        with dataset.connect() as tx:
            tx[source].insert(
                dict(currency=currency, rate=_json['payload']['last'], timestamp=time.time())
            )
    else:
        print("Updating price in database: " + currency)
        _table.update(
            dict(currency=currency, rate=_json['payload']['last'], timestamp=time.time()), ["currency"]
        )


def get_rate(currency, source):
    _db = dataset.connect()
    _table = _db[source]
    _record = _table.find_one(currency=currency)
    _now = time.time()

    if source in SOURCES:
        if not _record:
            print("TABLE: {} ENTRY: {} NOT FOUND - UPDATING".format(source, currency))
            update_db(source, currency)
            _record = _table.find_one(currency=currency)
        else:
            if (_now - _record['timestamp']) > 30:
                print("TABLE {} OUTDATED - UPDATING".format(source))
                update_db(source, currency)
                _record = _table.find_one(currency=currency)
            else:
                _record = _table.find_one(currency=currency)

    return _record
