# Database
import dataset
from stuf import stuf


def connect():
    db = dataset.connect(row_type=stuf)

    if "payment_requests" not in db:
        print("payment_request table missing, generating...")
        table = db["payment_requests"]
        table.create_column("timestamp", db.types.float)
        table.create_column("ip", db.types.text)
        table.create_column("addr", db.types.text)
        table.create_column("amount", db.types.text)
        table.create_column("label", db.types.text)
        table.create_column("received", db.types.integer)
        table.create_column("confirmations", db.types.integer)
        table.create_column("txid", db.types.text)
        table.create_column("order", db.types.integer)

    if "bitso" not in db:
        print("Bitso table missing, generating...")
        table = db["bitso"]
        table.create_column("currency", db.types.text)
        table.create_column("rate", db.types.float)
        table.create_column("timestamp", db.types.float)

    if "order_items" not in db:
        print("order_items table missing, generating...")
        table = db["order_items"]
        table.create_column("label", db.types.text)
        table.create_column("timestamp", db.types.text)
        table.create_column("item_id", db.types.integer)
        table.create_column("name", db.types.text)
        table.create_column("currency", db.types.text)
        table.create_column("bch", db.types.text)
        table.create_column("count", db.types.integer)
        table.create_column("exchange_rate", db.types.text)

    if "orders" not in db:
        print("orders table missing, generating...")
        table = db["orders"]
        table.create_column("label", db.types.text)
        table.create_column("timestamp", db.types.float)
        table.create_column("type", db.types.text)
        table.create_column("data", db.types.text)

    return db
