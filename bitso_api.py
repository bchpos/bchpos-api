import hashlib
import hmac
import json
import os
import time
from urllib.parse import urlparse, urlencode

import requests

_base_url_v2: str = ""
_base_url: str = ""
_key: str = ""
_secret: str = ""

OUTLETS = [
    {
        "available": "1",
        "daily_limit": "0.00",
        "fee": "0.00",
        "id": "bch",
        "maximum_transaction": "0.00",
        "minimum_transaction": "1.00",
        "name": "Bitcoin Cash",
        "net": "0.00",
        "optional_fields": [],
        "required_fields": [],
        "verification_level_requirement": "0"
    },
    {
        "available": "1",
        "daily_limit": "2500000.00",
        "fee": "0.00",
        "id": "sp",
        "maximum_transaction": "2500000.00",
        "minimum_transaction": "1.00",
        "name": "Transferencia SPEI",
        "net": "1000.00",
        "optional_fields": [
            {
                "id": "notes_ref",
                "name": "Notas de referencia"
            },
            {
                "id": "numeric_ref",
                "name": "Número de referencia"
            },
            {
                "id": "bank_name",
                "name": "Nombre de institución financiera"
            },
            {
                "id": "rfc",
                "name": "RFC"
            }
        ],
        "required_fields": [
            {
                "id": "recipient_given_names",
                "name": "Nombre del beneficiario"
            },
            {
                "id": "recipient_family_names",
                "name": "Apellidos del beneficiario"
            },
            {
                "id": "clabe",
                "name": "CLABE interbancaria"
            }
        ],
        "verification_level_requirement": "1"
    },
]


def get_outlets():
    return {"outlets": OUTLETS}


def current_milli_time():
    nonce = str(int(round(time.time() * 1000000)))
    return nonce


def _build_auth_header(http_method, url, json_payload=''):
    if json_payload == {} or json_payload == '{}':
        json_payload = ''
    url_components = urlparse(url)
    request_path = url_components.path
    if url_components.query != '':
        request_path += '?' + url_components.query
    nonce = current_milli_time()
    msg_concat = nonce + http_method.upper() + request_path + json_payload
    signature = hmac.new(_secret.encode('utf-8'),
                         msg_concat.encode('utf-8'),
                         hashlib.sha256).hexdigest()
    return {'Authorization': 'Bitso %s:%s:%s' % (_key, nonce, signature)}


def _encode_parameters(parameters):
    if parameters is None:
        return None
    else:
        param_tuples = []
        for k, v in parameters.items():
            if v is None:
                continue
            if isinstance(v, (list, tuple)):
                for single_v in v:
                    param_tuples.append((k, single_v))
            else:
                param_tuples.append((k, v))
        return urlencode(param_tuples)


def _build_url(url, params):
    if params and len(params) > 0:
        url = url + '?' + _encode_parameters(params)
    return url


def _check_for_api_error(data):
    if data['success'] != True:
        raise Exception(data['error'])
    if 'error' in data:
        raise Exception(data['error'])
    if isinstance(data, (list, tuple)) and len(data) > 0:
        if 'error' in data[0]:
            raise Exception(data[0]['error'])


def _parse_json(json_data):
    try:
        data = json.loads(json_data)
        _check_for_api_error(data)
    except:
        raise
    return data


def _request_url(url, verb, params=None, private=False):
    global resp
    headers = None
    if params == None:
        params = {}
    params = {k: v.decode("utf-8") if isinstance(v, bytes) else v for k, v in params.items()}
    if private:
        headers = _build_auth_header(verb, url, json.dumps(params))
    if verb == 'GET':
        url = _build_url(url, params)
        if private:
            headers = _build_auth_header(verb, url)
        try:
            resp = requests.get(url, headers=headers)
        except requests.RequestException as e:
            raise
    elif verb == 'POST':
        try:
            resp = requests.post(url, json=params, headers=headers)
        except requests.RequestException as e:
            raise
    elif verb == 'DELETE':
        try:
            resp = requests.delete(url, headers=headers)
        except requests.RequestException as e:
            raise

    content = resp.content
    data = _parse_json(content if isinstance(content, str) else content.decode('utf-8'))
    return data


def init():
    print("Initializing Bitso API - ENV=" + os.environ["FLASK_ENV"])
    global _base_url
    global _base_url_v2
    global _key
    global _secret

    if os.environ["FLASK_ENV"] == "development":
        _base_url_v2 = "https://dev.bitso.com/api/v2"
        _base_url = "https://dev.bitso.com/api/v3"
        _key = os.environ["BITSO_API_DEV_KEY"]
        _secret = os.environ["BITSO_API_DEV_SECRET"]
    else:
        _base_url_v2 = "https://api.bitso.com/v2"
        _base_url = "https://api.bitso.com/v3"
        _key = os.environ["BITSO_API_KEY"]
        _secret = os.environ["BITSO_API_SECRET"]


def available_books():
    _url = '%s/available_books/' % _base_url
    _resp = _request_url(_url, 'GET')
    return _resp


def ticker(book=None):
    _url = '%s/ticker/' % _base_url
    parameters = {}
    if book is not None:
        parameters['book'] = book
    _resp = _request_url(_url, 'GET', params=parameters)
    return _resp['payload']


def order_book(book, aggregate=True):
    _url = '%s/order_book/' % _base_url
    parameters = {}
    parameters['book'] = book
    parameters['aggregate'] = aggregate
    _resp = _request_url(_url, 'GET', params=parameters)
    return _resp['payload']


def trades(book, marker, sort, limit):
    _url = '%s/trades/' % _base_url
    parameters = {}
    parameters['book'] = book
    parameters['marker'] = marker
    parameters['limit'] = limit
    parameters['sort'] = sort
    _resp = _request_url(_url, 'GET', params=parameters)
    return _resp['payload']


def tranfer_quote(from_amount=None, to_amount=None):
    if from_amount is None and to_amount is None:
        raise AssertionError({u'message': u"Neither 'from_amount' nor 'to_amount' are specified"})
    if from_amount is not None and to_amount is not None:
        raise AssertionError({u'message': u"'from_amount' and 'to_amount' are mutually exclusive. Pick one"})

    url = '%s/transfer_quote' % _base_url
    parameters = {}
    if from_amount:
        parameters['from_amount'] = str(from_amount)
    elif to_amount:
        parameters['to_amount'] = str(to_amount)

    parameters['from_currency'] = 'BCH'
    parameters['to_currency'] = 'MXN'
    parameters['full'] = True

    _resp = _request_url(url, 'POST', params=parameters, private=True)
    return _resp['payload']


def transfer_create(from_amount=None,
                    to_amount=None,
                    rate=None,
                    payment_outlet=None,
                    **kwargs):
    if from_amount is None and to_amount is None:
        raise AssertionError({u'message': u"Neither 'from_amount' nor 'to_amount' are specified"})
    if from_amount is not None and to_amount is not None:
        raise AssertionError({u'message': u"'from_amount' and 'to_amount' are mutually exclusive. Pick one"})
    if rate is None:
        raise AssertionError({u'message': u"'rate' not specified"})
    if payment_outlet is None:
        raise AssertionError({u'message': u"'payment_outlet' not specified"})

    url = '%s/transfer_create' % _base_url
    parameters = {}
    if from_amount:
        parameters['from_amount'] = str(from_amount)
    elif to_amount:
        parameters['to_amount'] = str(to_amount)

    parameters['from_currency'] = 'BCH'
    parameters['to_currency'] = 'MXN'
    parameters['rate'] = str(rate)
    parameters['payment_outlet'] = payment_outlet
    for k, v in kwargs.items():
        parameters[k] = str(v).encode('utf-8')
    _resp = _request_url(url, 'POST', params=parameters, private=True)
    return _resp['payload']
