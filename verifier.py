import os
import random

import requests
from cashaddress import convert

dev_explorers = [
    {
        "api_addr": "https://trest.bitcoin.com/v2/address/details/{caddr}",
        "api_tx": "https://trest.bitcoin.com/v2/transaction/details/"
    }
]

explorers = [
    {
        "api_addr": "https://rest.bitcoin.com/v2/address/details/{caddr}",
        "api_tx": "https://rest.bitcoin.com/v2/transaction/details/"
    }
]


def randomize():
    if os.environ["FLASK_ENV"] == "development":
        rand = random.randint(0, len(dev_explorers) - 1)
        api = dev_explorers[rand]
    else:
        rand = random.randint(0, len(explorers) - 1)
        api = explorers[rand]

    return api


def get_api_addr(addr):
    _legacy = convert.to_legacy_address(addr)
    _addr = {"caddr": addr, "legacy": _legacy}

    _api = randomize()

    _request = _api["api_addr"].format(**_addr)
    print(_request)
    _r = requests.get(_request)
    _json_addr = _r.json()

    return _json_addr


def get_api_tx(tx):
    _api = randomize()
    _request = _api["api_tx"] + tx
    print(_request)
    _r = requests.get(_request)
    _json_tx = _r.json()

    return _json_tx


def verify_tx(addr):
    _legacy = convert.to_legacy_address(addr)
    _addr = {"caddr": addr, "legacy": _legacy}
    _json_addr = get_api_addr(addr)
    _amount = (
        _json_addr["unconfirmedBalance"]
        if _json_addr["unconfirmedBalance"] != 0
        else _json_addr["totalReceived"]
    )

    # print(json.dumps(_json_addr, indent=4))

    if len(_json_addr["transactions"]) == 1:
        print("tx: " + _json_addr["transactions"][0])
        _txid = _json_addr["transactions"][0]
        _api = randomize()
        _request = _api["api_tx"] + _txid
        _r = requests.get(_request)
        _json_tx = _r.json()
        # print(json.dumps(_json_tx, indent=4))
        for _out in _json_tx["vout"]:
            if float(_out["value"]) == _amount:
                if (
                        _legacy == _out["scriptPubKey"]["addresses"][0]
                        or addr.lstrip("bitcoincash:")
                        == _out["scriptPubKey"]["addresses"][0]
                ):
                    print("FOUND!")
                    print(_out["value"])
                    _tx = {
                        "value": _amount,
                        "input_address": addr,
                        "confirmations": _json_tx["confirmations"],
                        "transaction_hash": _txid,
                        "input_transaction_hash": _txid,
                        "destination_address": addr,
                    }
                    return _tx

    return False


def verify_tx_with_amount(addr, amount):
    _legacy = convert.to_legacy_address(addr)

    _json_addr = get_api_addr(addr)

    # print(json.dumps(_json_addr, indent=4))

    if len(_json_addr["transactions"]) > 0:
        for _txid in _json_addr["transactions"]:
            print("txid: " + _txid)
            _json_tx = get_api_tx(_txid)
            # print(json.dumps(_json_tx, indent=4))
            for _out in _json_tx["vout"]:
                if float(_out["value"]) == float(amount):
                    if (
                            _legacy == _out["scriptPubKey"]["addresses"][0]
                            or addr.lstrip("bitcoincash:")
                            == _out["scriptPubKey"]["addresses"][0]
                    ):
                        print("FOUND!")
                        print(_out["value"])
                        _tx = {
                            "value": amount,
                            "input_address": addr,
                            "confirmations": _json_tx["confirmations"],
                            "transaction_hash": _txid,
                            "input_transaction_hash": _txid,
                            "destination_address": addr,
                        }
                        return _tx

    return False


def verify(addr, amount):
    _legacy = convert.to_legacy_address(addr)
    _json_addr = get_api_addr(addr)

    if _json_addr["unconfirmedBalance"] == float(amount) or _json_addr["balance"] == float(amount):
        for _txid in _json_addr["transactions"]:
            print("txid: " + _txid)
            _json_tx = get_api_tx(_txid)
            for _out in _json_tx["vout"]:
                if float(_out["value"]) == float(amount):
                    if (
                            _legacy == _out["scriptPubKey"]["addresses"][0]
                            or addr.lstrip("bitcoincash:")
                            == _out["scriptPubKey"]["addresses"][0]
                    ):
                        print("FOUND!")
                        print(_out["value"])
                        _tx = {
                            "received": 1,
                            "receive_address": addr,
                            "amount": amount,
                            "confirmations": _json_tx["confirmations"],
                            "txid": _txid,
                        }
                        return _tx

    return {"received": 0}


def callback(addr, callbackurl, amount=False):
    _tx = False
    _callback = callbackurl
    if addr and amount:
        _tx = verify_tx_with_amount(addr, amount)
    else:
        _tx = verify_tx(addr)

    if _tx:
        _callback += "&value=" + _tx["value"]
        _callback += "&input_address=" + _tx["input_address"]
        _callback += "&confirmations" + _tx["confirmations"]
        _callback += "&transaction_hash" + _tx["transaction_hash"]
        _callback += "&input_transaction_hash" + _tx["input_transaction_hash"]
        _callback += "&destination_address" + _tx["destination_address"]

        _r = requests.get(_callback)
        return True
    else:
        return False
