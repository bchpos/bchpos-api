import io
import json
import os
import re
import sys
import time
from datetime import datetime
from datetime import timedelta

import dataset
import qrcode
from cashaddress import convert
from dotenv import load_dotenv
from flask import Flask, request, make_response, render_template, abort
from flask_restplus import Resource, Api, fields
from pycoin.key import Key
from werkzeug.middleware.proxy_fix import ProxyFix

import bitso_api
import exchangerate
import verifier
from bitso_api import OUTLETS

load_dotenv(verbose=True)

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)

api = Api(
    app, version="0.1", title="BCHPoS API", description="Bitcoin Cash Point of Sale API"
)

backendAPI = api.namespace('api', description='BCHPoS backend API')
bitsoAPI = api.namespace('bitso', description='Bitso API')

order = backendAPI.model(
    "Order",
    {
        "label": fields.String(required=False, description="Order Label"),
        "items": fields.List(fields.String, required=False, description="Order Items"),
        "subtotalBCH": fields.String(required=False, description="BCH Subtotal"),
        "discount": fields.String(required=False, description="Discount"),
        "discountBCH": fields.String(required=False, description="BCH Discount"),
        "tax": fields.String(required=False, description="Tax"),
        "taxBCH": fields.String(required=False, description="BCH Tax"),
        "totalBCH": fields.String(required=False, description="BCH Total"),
        "exchangeRATE": fields.String(required=False, description="Exchange rate"),
        "currency": fields.String(required=False, description="Currency")
    }
)


def headers(resp):
    if "HTTP_ORIGIN" in request.environ:
        origin = request.environ["HTTP_ORIGIN"]
        resp.headers["Access-Control-Allow-Origin"] = str(origin)
        resp.headers["Access-Control-Allow-Credentials"] = "true"
        resp.headers["Access-Control-Allow-Methods"] = "GET, POST, OPTIONS"
        resp.headers["Access-Control-Allow-Headers"] = "Content-Type"
        resp.headers[
            "Access-Control-Request-Headers"
        ] = "X-Requested-With, accept, content-type"


# Set initial wallet index based on the number of payments already received
def get_wallet_index():
    _db = dataset.connect()
    _id = len(_db["payment_requests"])
    return _id


# Read the public key from key.list file
def get_xpub():
    _xpub = 0
    try:
        f = open("key.list")
        for line in f.readlines():
            _key = line.strip()
            regexes = [re.compile("xpub"), re.compile("tpub")]
            if any(regex.match(_key) for regex in regexes):
                _xpub = Key.from_text(_key)
                f.close()
                return _xpub
        f.close()
        if _xpub == 0:
            print("ERROR: No public key in key.list")
            sys.exit(2)
    except (IOError, OSError, FileNotFoundError, PermissionError):
        print("ERROR: Could not open key.list")
        sys.exit(2)


# Derive address from xpub key and increment address index
def get_xpub_address(xpub, index):
    # 0 => public addresses
    # 1 => change addresses, only for internal use
    account_type = 0
    xpub_subkey = xpub.subkey(account_type)
    addr = xpub_subkey.subkey(index).bitcoin_address()
    caddr = convert.to_cash_address(addr)
    return caddr


def get_order(label):
    _db = dataset.connect()
    _table = _db["orders"]
    _order = _table.find(label=label)

    return _order


def get_order_items(label):
    _db = dataset.connect()
    _table = _db["order_items"]
    _items = _table.find(label=label)

    return _items


def get_payment_by_label(label):
    _db = dataset.connect()
    _table = _db["payment_requests"]
    _payment = _table.find_one(label=label)
    if _payment:
        return _payment
    else:
        return False


def get_payment_by_addr(addr):
    _db = dataset.connect()
    _table = _db["payment_requests"]
    _payment = _table.find_one(addr=addr)
    if _payment:
        return _payment
    else:
        return False


# Generate QR code and return the image
def get_qr(parameters):
    _addr = False
    _amount = False
    _label = False
    _data = ""

    if "addr" in parameters:
        _addr = parameters.get("addr").upper()
        # _addr = parameters['addr'][0].rstrip('/')
        _data = _addr
    else:
        abort(401, "Specify address")

    if "amount" in parameters and "label" in parameters:
        _amount = parameters.get("amount")
        _label = parameters.get("label")
        _data += "?amount=" + _amount
        _data += "&message=" + _label
    elif "amount" in parameters:
        _amount = parameters.get("amount")
        _data += "?amount=" + _amount
    elif "label" in parameters:
        _label = parameters.get("label")
        _data += "?message=" + _label

    _data += "&editable=false"

    # print(_data)

    qr = qrcode.QRCode(error_correction=qrcode.constants.ERROR_CORRECT_L, border=2)
    qr.add_data(_data)
    qr.make(fit=True)
    img = qr.make_image()

    return img


def create_payment_request(ip_addr, amount="", label="", address=None):
    _order = 0

    if label:
        _db = dataset.connect()
        _table = _db["order_items"]
        _item = _table.find_one(label=label)
        if _item:
            _order = 1

    if address is None:
        _xpub = get_xpub()
        _index = get_wallet_index()
        _addr = get_xpub_address(_xpub, _index)
    else:
        _addr = address

    with dataset.connect() as tx:
        tx["payment_requests"].insert(
            dict(
                timestamp=time.time(),
                ip=ip_addr,
                addr=_addr,
                amount=amount,
                label=label,
                received=0,
                confirmations=0,
                txid="NoTX",
                order=_order,
            )
        )
    return _addr


def generate_payment(parameters, ip_addr):
    parameters = parameters.to_dict()
    if "amount" not in parameters:
        abort(400, "Specify an amount!")

    _amount = parameters.pop("amount")
    _label = False
    _payment = False
    _uri = ""
    _uri_params = "?amount=" + _amount
    _qr = "/api/qr?addr="
    _qr_params = "&amount=" + _amount

    if "label" in parameters:
        _label = parameters.pop("label")
        _qr_params += "&label=" + _label
        _uri_params += "&message=" + _label

    if "outlet" in parameters:
        _outlet_id = parameters.pop("outlet")
        _outlet = next((outlet for outlet in OUTLETS if outlet['id'] == _outlet_id), None)
        if _outlet['id'] == 'sp':
            _quote = bitso_api.tranfer_quote(from_amount=_amount)
            _rate = _quote['rate']
            _transfer = bitso_api.transfer_create(from_amount=_amount, rate=_rate, payment_outlet=_outlet, **parameters)
            _expires = _transfer['expires_at']
            _addr = create_payment_request(ip_addr=ip_addr, amount=_amount, label=_label,
                                           address=_transfer['wallet_address'])
        else:
            _expires = (datetime.now() + timedelta(seconds=60)).timestamp()
            _addr = create_payment_request(ip_addr=ip_addr, amount=_amount, label=_label)
    else:
        _expires = (datetime.now() + timedelta(seconds=60)).timestamp()
        _addr = create_payment_request(ip_addr=ip_addr, amount=_amount, label=_label)

    _legacy = convert.to_legacy_address(_addr)
    _qr += _addr + _qr_params
    _uri = _addr + _uri_params + "&editable=false"

    if os.environ["FLASK_ENV"] == "development":
        _explorer_url = "https://explorer.bitcoin.com/tbch/address/"
    else:
        _explorer_url = "https://explorer.bitcoin.com/bch/address/"

    _payreq = {
        "payment": {
            "amount": _amount,
            "addr": _addr,
            "legacy_addr": _legacy,
            "label": _label,
            "qr_img": _qr,
            "payment_uri": _uri,
            "explorer_url": _explorer_url,
            "expires": _expires
        }
    }

    # _json = json.dumps(_payreq, default=str)
    return _payreq


def update_payment_received(payment_id, received, txid):
    _db = dataset.connect()
    _table = _db["payment_requests"]

    _table.update(dict(id=payment_id, received=received, txid=txid), ["id"])


def generate_embed(addr, parameters):
    _amount = False
    _label = False
    _qruri = "/api/qr?addr=" + addr
    _copy = addr
    _amount_label = ""

    if "amount" in parameters:
        _amount = parameters.get("amount")
        _qruri += "&amount=" + _amount
        _amount_label += "AMOUNT: " + _amount + " BCH "
        _copy += "?amount=" + _amount
    if "label" in parameters:
        _label = parameters.get("label")
        _qruri += "&label=" + _label
        _amount_label += "LABEL: " + _label + " "
        if _amount:
            _copy += "&message=" + _label
        else:
            _copy += "?message=" + _label

    filler = {"addr": addr, "qruri": _qruri, "label": _amount_label, "copy": _copy}
    html = render_template("qr.html").format(**filler)

    return html


def generate_verify(parameters, ip_addr):
    _addr = False
    _amount = False
    _label = False
    _received = False
    _payment = False

    # Find payment by label, if not verified, set parameters
    if "label" in parameters:
        _payment = get_payment_by_label(parameters.get("label"))
        if not _payment:
            abort(400, "ERROR: Unknown Label! - " + parameters.get("label"))
    # Find payment by addr, if not verified, set parameters
    elif "addr" in parameters and "amount" in parameters:
        _amount = parameters.get("amount")
        if convert.is_valid(parameters.get("addr")):
            _addr = parameters.get("addr")
            _payment = get_payment_by_addr(_addr)
            if not _payment:
                abort(400, "ERROR: Unknown Payment Address! - " + _addr)
        else:
            abort(400, "Invalid address!")
    else:
        abort(400, "Incorrect use of API, RTFM!")

    # TODO: return more payment details. see verify_tx.verify()
    if _payment['received'] == 1:
        return json.dumps({"received": 1})
    else:
        _addr = _payment['addr']
        _amount = _payment['amount']
        _label = _payment['label']

    # If payment not received yet, rescan
    _received = verifier.verify(_addr, _amount)

    # If rescan has positive result, update payment record in database
    if _received["received"] == 1 and _payment['received'] == 0:
        update_payment_received(_payment['id'], 1, _received["txid"])

    # Return verification result
    if _received:
        return json.dumps(_received)
    else:
        return json.dumps({"received": 0})


def generate_order(parameters, ip_addr):
    if "label" in parameters:
        _order = get_order(parameters.get("label"))
        _items = get_order_items(parameters.get("label"))
        order = {}
        order["label"] = parameters.get("label")
        items = []

        for _item in _items:
            item = {
                "id": _item.item_id,
                "timestamp": _item.timestamp,
                "name": _item.name,
                "currency": _item.currency,
                "bch": _item.bch,
                "count": _item.count,
                "exchangeRATE": _item.exchange_rate,
            }
            items.append(item)

        for _element in _order:
            # print(_element.label, _element.type)
            order["timestamp"] = _element.timestamp
            order[_element.type] = _element.data

        order["items"] = items

        # print(order)
        return order

    return {"error": 0}


def generate_ledger():
    _db = dataset.connect()
    _ledger = {}
    _ledger['ledger'] = {}

    for _payment in _db["payment_requests"]:
        _data = {
            "id": _payment['id'],
            "timestamp": _payment['timestamp'],
            "addr": _payment['addr'],
            "amount": _payment['amount'],
            "label": _payment['label'],
            "received": _payment['received'],
            "confirmations": _payment['confirmations'],
            "txid": _payment['txid'],
            "order": _payment['order'],
        }
        _ledger['ledger'][str(_payment['id'])] = _data

    if os.environ["FLASK_ENV"] == "development":
        _ledger['tx_explorer'] = "https://explorer.bitcoin.com/tbch/tx/"
        _ledger['addr_explorer'] = "https://explorer.bitcoin.com/tbch/address/"
    else:
        _ledger['tx_explorer'] = "https://explorer.bitcoin.com/bch/tx/"
        _ledger['addr_explorer'] = "https://explorer.bitcoin.com/bch/address/"

    # _json = json.dumps(_ledger)
    return _ledger


def generate_rate(parameters):
    _currency = ""
    _source = ""
    if "currency" in parameters and "source" in parameters:
        if not exchangerate.is_supported(
                parameters.get("currency"), parameters.get("source")
        ):
            abort(400, "ERROR: Unsupported Currency! => " + parameters.get("currency"))
        else:
            _r = exchangerate.get_rate(
                parameters.get("currency"), parameters.get("source")
            )
            _price = {"currency": _r['currency'], "price": _r['rate']}
            return _price
    # TODO: Error handling is_supported()
    elif "source" in parameters:
        _r = exchangerate.get_currencies(parameters.get("source"))
        return _r
    else:
        _r = exchangerate.get_sources()
        return _r


@app.before_first_request
def before_first_request_func():
    index = get_wallet_index()
    xpub = get_xpub()
    bitso_api.init()
    print("Wallet using xpub key: " + xpub.as_text()[:20] + "..." + xpub.as_text()[-8:])
    print("Starting wallet at index: " + str(index))
    # app.run(debug=True, host='0.0.0.0', port=8080, thread_count=3)


@backendAPI.route('/ping')
class Ping(Resource):
    def get(self):
        _pong = {'pong': 1}
        resp = make_response(_pong)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@backendAPI.route("/order")
class Order(Resource):
    @backendAPI.doc(
        description="Gets an order.",
        params={
            "label": "Label"
        }
    )
    def get(self):
        _ip = request.environ.get("REMOTE_ADDR")
        _parameters = request.values
        _order = generate_order(_parameters, _ip)

        resp = make_response(_order)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp

    @backendAPI.doc(
        description="Generates an order."
    )
    @backendAPI.expect(order)
    def post(self):
        _now = time.time()
        _json = request.json

        label = _json["label"]
        items = _json["items"]
        subtotalBCH = _json["subtotalBCH"]

        discount = _json["discount"]
        discountBCH = _json["discountBCH"]
        tax = _json["tax"]
        taxBCH = _json["taxBCH"]

        totalBCH = _json["totalBCH"]
        exchangeRATE = _json["exchangeRATE"]
        currency = _json["currency"]

        for key in _json:
            if key == "items":
                for item in _json[key]:
                    print(
                        "INSERT item   ("
                        + label
                        + "): "
                        + item["name"]
                        + " => "
                        + item["bch"]
                    )
                    with dataset.connect() as tx:
                        tx["order_items"].insert(
                            dict(
                                label=label,
                                timestamp=item["timestamp"],
                                item_id=item["id"],
                                name=item["name"],
                                currency=item["currency"],
                                bch=item["bch"],
                                count=item["count"],
                                exchange_rate=item["exchangeRATE"],
                            )
                        )
            elif key != "label" and key != "items":
                print("INSERT order (" + label + "): " + key + " => " + str(_json[key]))
                with dataset.connect() as tx:
                    tx["orders"].insert(
                        dict(label=label, timestamp=_now, type=key, data=_json[key])
                    )

        resp = make_response(_json)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@backendAPI.route("/embed")
class Embed(Resource):
    def get(self):
        _ip = request.environ.get("REMOTE_ADDR")
        _parameters = request.values

        _addr = create_payment_request(_ip)
        _html = generate_embed(_addr, _parameters)

        resp = make_response(_html)
        headers(resp)
        resp.headers["Content-type"] = "text/html; charset=utf-8"
        return resp


@backendAPI.route("/qr")
@backendAPI.doc(
    description="Generates QR of payment request.",
    params={
        "addr": "Bitcoin Cash address",
        "amount": "Amount",
        "label": "Label"
    }
)
class QR(Resource):
    def get(self):
        _parameters = request.values
        _qr = get_qr(_parameters)
        _output = io.BytesIO()
        _qr.save(_output, format="PNG")

        resp = make_response(_output.getvalue())
        headers(resp)
        resp.headers["Content-type"] = "image/png"
        return resp


@backendAPI.route("/payment")
@backendAPI.doc(
    description="Generate new payment request.",
    params={
        "amount": "Amount",
        "label": "Label",
        "outlet": "Payment outlet if planning to cash out"
    }
)
class Payment(Resource):
    def get(self):
        _ip = request.environ.get("REMOTE_ADDR")
        _parameters = request.values
        _payment = generate_payment(_parameters, _ip)

        resp = make_response(_payment)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@backendAPI.route("/verify")
@backendAPI.doc(
    description="Verify payment of payment request.",
    params={
        "addr": "Bitcoin Cash address",
        "amount": "Amount",
        "label": "Label"
    }
)
class Verify(Resource):
    def get(self):
        _ip = request.environ.get("REMOTE_ADDR")
        _parameters = request.values
        _verify = generate_verify(_parameters, _ip)

        resp = make_response(_verify)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@backendAPI.route("/ledger")
@backendAPI.doc(
    description="Fetch sales ledger."
)
class Ledger(Resource):
    def get(self):
        _ledger = generate_ledger()

        resp = make_response(_ledger)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@backendAPI.route("/rate")
@backendAPI.doc(
    description="Fetch exchange rate or list supported currencies for specified source.",
    params={
        "source": "Exchange site to get the rate from",
        "currency": "Currency"
    }
)
class Rate(Resource):
    def get(self):
        _parameters = request.values
        _rate = generate_rate(_parameters)

        resp = make_response(_rate)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@bitsoAPI.route("/available_books")
@bitsoAPI.doc(
    description="Order books available on Bitso."
)
class AvailableBooks(Resource):
    def get(self):
        _json = bitso_api.available_books()
        resp = make_response(_json)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@bitsoAPI.route("/ticker")
@bitsoAPI.doc(
    description="Ticker information.",
    params={
        "book": "Specifies which book to use"
    }
)
class Ticker(Resource):
    def get(self):
        _parameters = request.values
        _book = _parameters.get("book")

        _json = bitso_api.ticker([_book, "bch_mxn"][_book is None])
        resp = make_response(_json)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@bitsoAPI.route("/order_book")
@bitsoAPI.doc(
    description="Public order book.",
    params={
        "book": "Specifies which book to use",
        "aggregate": "Group orders with the same price"
    }
)
class OrderBook(Resource):
    def get(self):
        _parameters = request.values
        _book = _parameters.get("book")
        _aggregate = _parameters.get("aggregate")

        _json = bitso_api.order_book(
            [_book, "bch_mxn"][_book is None], [_aggregate, False][_aggregate is None]
        )
        resp = make_response(_json)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@bitsoAPI.route("/trades")
@bitsoAPI.doc(
    description="Public order book.",
    params={
        "book": "Specifies which book to use",
        "marker": "Returns objects that are older or newer (depending on 'sort’) than the object with this ID",
        "sort": "Specifies ordering direction of returned objects (asc, desc)",
        "limit": "Specifies number of objects to return. (Max is 100)"
    }
)
class Trades(Resource):
    def get(self):
        _parameters = request.values
        _book = _parameters.get("book")
        _marker = _parameters.get("marker")
        _sort = _parameters.get("sort")
        _limit = _parameters.get("limit")

        _json = bitso_api.trades(
            [_book, "bch_mxn"][_book is None],
            _marker,
            [_sort, "desc"][_sort is None],
            [_limit, "25"][_limit is None],
        )

        resp = make_response(_json)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@bitsoAPI.route("/transfer_quote")
@bitsoAPI.doc(
    description="Get a quote for a transfer for various Bitso Outlets.",
    params={
        "from_amount": "Mutually exclusive with to_amount. Either this, or to_amount should be present in the request. The total amount in Crypto, as provided by the user.",
        "to_amount": "Mutually exclusive with from_amount. Either this, or from_amount should be present in the request. The total amount in Fiat currency. Use this if you prefer specifying amounts in fiat instead of cryptocurrency."
    }
)
class TransferQuote(Resource):
    def get(self):
        _parameters = request.values
        _from_amount = _parameters.get("from_amount")
        _to_amount = _parameters.get("to_amount")

        _json = bitso_api.tranfer_quote(_from_amount, _to_amount)
        resp = make_response(_json)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@bitsoAPI.route("/create_transfer")
@bitsoAPI.doc(
    description="Create a transfer based on a previous quote's rate.",
    params={
        "from_amount": "Mutually exclusive with to_amount. Either this, or to_amount should be present in the request. The total amount in Crypto, as provided by the user.",
        "to_amount": "Mutually exclusive with from_amount. Either this, or from_amount should be present in the request. The total amount in Fiat currency. Use this if you prefer specifying amounts in fiat instead of cryptocurrency.",
        "rate": "This is the rate (e.g. BTC/MXN), as acquired from the transfer_quote method. You must request a quote in this way before creating a transfer.",
        "payment_outlet": "The outlet_id as provided by quote method.",
        "recipient_given_names": "The recipient’s first and middle name(s).",
        "recipient_family_names": "The recipient’s last name.",
        "clabe": "The CLABE number where the funds will be sent to.",
        "email": "The recipient's email address."
    }
)
class CreateTransfer(Resource):
    def get(self):
        _parameters = request.values
        _from_amount = _parameters.get("from_amount")
        _to_amount = _parameters.get("to_amount")
        _rate = _parameters.get("rate")
        _payment_outlet = _parameters.get("payment_outlet")

        _json = bitso_api.transfer_create(_from_amount, _to_amount, _rate, _payment_outlet)
        resp = make_response(_json)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


@bitsoAPI.route("/outlet")
@bitsoAPI.doc(
    description="Get available payment outlets and required fields of each."
)
class Outlet(Resource):
    def get(self):
        _json = bitso_api.get_outlets()
        resp = make_response(_json)
        headers(resp)
        resp.headers["Content-type"] = "application/json"
        return resp


if __name__ == "__main__":
    app.run()
